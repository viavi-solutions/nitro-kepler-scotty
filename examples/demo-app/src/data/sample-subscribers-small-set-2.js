// Copyright (c) 2019 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// Sample Subscriber Route Data
export default {
  fields: [
    {
      name: 'subscriber_measurement_datetime',
      format: 'YYYY-M-D H:m:s',
      tableFieldIndex: 1,
      type: 'timestamp'
    },
    {
      name: 'subscriber_imsi',
      format: '',
      tableFieldIndex: 2,
      type: 'string'
    },
    {name: 'subscriber_latitude', format: '', tableFieldIndex: 3, type: 'real'},
    {name: 'subscriber_longitude', format: '', tableFieldIndex: 4, type: 'real'},
    {name: 'subscriber_elevation', format: '', tableFieldIndex: 5, type: 'real'},
    {name: 'coverage_beam_uid', format: '', tableFieldIndex: 6, type: 'real'},
    {name: 'coverage_beam_quality', format: '', tableFieldIndex: 7, type: 'real'},
    {name: 'subscriber_beam_uid', format: '', tableFieldIndex: 8, type: 'real'},
    {name: 'subscriber_beam_quality', format: '', tableFieldIndex: 9, type: 'real'},
    {name: 'subscriber_beam_quality_color', format: '', tableFieldIndex: 10, type: 'real'},
  ],
  rows: [
    [
      '2019-01-15 12:00:00 +00:00',
      '310150123456789',
      32.781029, -96.801047,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:00:10 +00:00',
      '310150123456789',
      32.781074083333333, -96.800831166666667,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:00:20 +00:00',
      '310150123456789',
      32.781119166666666, -96.800615333333334,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:00:30 +00:00',
      '310150123456789',
      32.781164249999999, -96.800399500000001,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:00:40 +00:00',
      '310150123456789',
      32.781209333333332, -96.800183666666668,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:00:50 +00:00',
      '310150123456789',
      32.781254416666665, -96.799967833333335,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:01:00 +00:00',
      '310150123456789',
      32.781299499999998, -96.799752000000002,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:01:10 +00:00',
      '310150123456789',
      32.781344583333331, -96.799536166666669,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:01:20 +00:00',
      '310150123456789',
      32.781389666666664, -96.799320333333336,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:01:30 +00:00',
      '310150123456789',
      32.781434749999997, -96.799104500000003,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:01:40 +00:00',
      '310150123456789',
      32.78147983333333, -96.79888866666667,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:01:50 +00:00',
      '310150123456789',
      32.781524916666663, -96.798672833333337,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:02:00 +00:00',
      '310150123456789',
      32.781569999999996, -96.798457000000004,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:02:10 +00:00',
      '310150123456789',
      32.781794,-96.798457,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      6
    ],
    [
      '2019-01-15 12:02:20 +00:00',
      '310150123456789',
      32.781898,-96.798216,
      0,
      310001,
      0.80,
      310001.001,
      0.95,
      0
    ],
    [
      '2019-01-15 12:02:30 +00:00',
      '310150123456789',
      32.781898,-96.798216,
      10,
      310001,
      0.80,
      310001.001,
      0.10,
      0
    ],
    [
      '2019-01-15 12:02:40 +00:00',
      '310150123456789',
      32.781898,-96.798216,
      30,
      310001,
      0.80,
      310001.001,
      0.10,
      0
    ],
    [
      '2019-01-15 12:02:50 +00:00',
      '310150123456789',
      32.781898,-96.798216,
      60,
      310001,
      0.80,
      310001.001,
      0.10,
      0
    ],
    [
      '2019-01-15 12:03:00 +00:00',
      '310150123456789',
      32.781898,-96.798216,
      110,
      310001,
      0.80,
      310001.001,
      0.10,
      0
    ],
    [
      '2019-01-15 12:03:10 +00:00',
      '310150123456789',
      32.781898,-96.798216,
      170,
      310001,
      0.80,
      310001.001,
      0.10,
      0
    ]
  ]
};
