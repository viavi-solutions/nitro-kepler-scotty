// Copyright (c) 2019 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// Sample Subscriber Route Data

export default {
  fields: [
    {
      name: 'cell-uid',
      format: '',
      tableFieldIndex: 1,
      type: 'real'
    },
    {name: 'cell_latitude', format: '', tableFieldIndex: 2, type: 'real'},
    {name: 'cell_longitude', format: '', tableFieldIndex: 3, type: 'real'},
    {name: 'cell_power', format: '', tableFieldIndex: 4, type: 'real'},
    {name: 'cell_elevation', format: '', tableFieldIndex: 5, type: 'real'},
    {name: 'coverage_uid', format: '', tableFieldIndex: 6, type: 'real'},
    {name: 'coverage_latitude', format: '', tableFieldIndex: 7, type: 'real'},
    {name: 'coverage_longitude', format: '', tableFieldIndex: 8, type: 'real'},
    {name: 'coverage_radius', format: '', tableFieldIndex: 9, type: 'real'},
    {name: 'coverage_color', format: '', tableFieldIndex: 9, type: 'real'},
    {name: 'coverage_type', format: '', tableFieldIndex: 9, type: 'real'}
  ],
  rows: [
    [
      'f81d4fae-7dec-11d0-a765-00a0c91e6bf6',
      32.780839,
      -96.800806,
      100,
      100,
      'f81d4fae-7dec-11d0-a765-00a0c91e6bf6',
      32.781200,
      -96.800380,
      75,
      [130,245,113],
      'coverage_beam'
    ],
    [
      'f81d4fae-7dec-11d0-a765-00a0c91e6bf7',
      32.780839,
      -96.800806,
      100,
      100,
      'f81d4fae-7dec-11d0-a765-00a0c91e6bf7.01',
      32.781506,-96.798878,
      75,
      [130,245,113],
      'coverage_beam'
    ]
  ]
};
