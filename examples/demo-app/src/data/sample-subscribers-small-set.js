// Copyright (c) 2019 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// Sample Subscriber Route Data
export default {
  fields: [
    {
      name: 'subscriber_measurement_datetime',
      format: 'YYYY-M-D H:m:s',
      tableFieldIndex: 1,
      type: 'timestamp'
    },
    {
      name: 'subscriber_imsi',
      format: '',
      tableFieldIndex: 2,
      type: 'string'
    },
    {name: 'subscriber_latitude', format: '', tableFieldIndex: 3, type: 'real'},
    {name: 'subscriber_longitude', format: '', tableFieldIndex: 4, type: 'real'},
    {name: 'coverage_beam_uid', format: '', tableFieldIndex: 5, type: 'real'},
    {name: 'coverage_beam_quality', format: '', tableFieldIndex: 6, type: 'real'},
    {name: 'subscriber_beam_uid', format: '', tableFieldIndex: 7, type: 'real'},
    {name: 'subscriber_beam_quality', format: '', tableFieldIndex: 8, type: 'real'}
  ],
  rows: [
    [
      '2019-01-15 12:00:00 +00:00',
      '310150123456789',
      32.781075,
      -96.800954,
      310001,
      0.80,
      310001.001,
      0.95
    ],
    [
      '2019-01-15 12:00:10 +00:00',
      '310150123456789',
      32.781112,
      -96.800777,
      310001,
      0.80,
      310001.001,
      0.95
    ],
    [
      '2019-01-15 12:00:20 +00:00',
      '310150123456789',
      32.781179,
      -96.800477,
      310001,
      0.80,
      310001.001,
      0.95
    ],
    [
      '2019-01-15 12:00:30 +00:00',
      '310150123456789',
      32.781214,
      -96.800261,
      310001,
      0.80,
      310001.001,
      0.95
    ],
    [
      '2019-01-15 12:00:40 +00:00',
      '310150123456789',
      32.781268,
      -96.800047,
      310001,
      0.80,
      310001.001,
      0.95
    ],
    [
      '2019-01-15 12:00:50 +00:00',
      '310150123456789',
      32.781256,
      -96.800145,
      310001,
      0.80,
      310001.001,
      0.95
    ],
    [
      '2019-01-15 12:01:00 +00:00',
      '310150123456789',
      32.781342,
      -96.799780,
      310001,
      0.80,
      310001.001,
      0.95
    ],
    [
      '2019-01-15 12:01:10 +00:00',
      '310150123456789',
      32.781405,
      -96.799394,
      310001,
      0.80,
      310001.001,
      0.95
    ],
    [
      '2019-01-15 12:01:20 +00:00',
      '310150123456789',
      32.781495,
      -96.798997,
      310001,
      0.80,
      310001.001,
      0.95
    ],
    [
      '2019-01-15 12:01:30 +00:00',
      '310150123456789',
      32.781561,
      -96.798594,
      310001,
      0.80,
      310001.001,
      0.95
    ]
  ]
};
