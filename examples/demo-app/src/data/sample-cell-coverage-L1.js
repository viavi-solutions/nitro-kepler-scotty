// Copyright (c) 2019 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// Sample Subscriber Route Data

let elevation = 10
let radius = 70
// 32.781181,-96.797959
let beam_cell = {
  elevation: 20,
  latitude: 32.781181,
  longitude: -96.797959,
  radius: 1
}

let user_beam = {
  elevation: 0,
  latitude: 32.781506,
  longitude: -96.798878,
  radius: 75
}

let delta = {
  elevation: (Math.abs(beam_cell.elevation) - Math.abs(user_beam.elevation)),
  latitude: (Math.abs(beam_cell.latitude) - Math.abs(user_beam.latitude))*-1,
  longitude: (Math.abs(beam_cell.longitude) - Math.abs(user_beam.longitude)),
  radius: (Math.abs(beam_cell.radius) - Math.abs(user_beam.radius))
}

console.log({delta:delta})

let step = {
  // elevation: delta.elevation,
  latitude: (delta.latitude / delta.elevation),
  longitude: (delta.longitude / delta.elevation),
  radius: (delta.radius / delta.elevation)

}

console.log({step:step});

let rows = []

let coveragePosition = {
  elevation: beam_cell.elevation,
  latitude: beam_cell.latitude,
  longitude: beam_cell.longitude,
  radius: beam_cell.radius
}

console.log({beam_cell_position:coveragePosition})

for(let i=beam_cell.elevation; i > user_beam.elevation ; i--){

  rows.push([
      'f81d4fae-7dec-11d0-a765-00a0c91e6bf7',
      beam_cell.latitude,
      beam_cell.longitude,
      100,
      100,
      'f81d4fae-7dec-11d0-a765-00a0c91e6bf7.01',
      coveragePosition.latitude,
      coveragePosition.longitude,
      coveragePosition.radius,
      [130,245,113],
      'coverage_beam',
      coveragePosition.elevation
  ])

  coveragePosition = {
    elevation: i,
    latitude: coveragePosition.latitude + step.latitude,
    longitude: coveragePosition.longitude + step.longitude,
    radius: coveragePosition.radius - step.radius
  }
}

console.log({userbeam_position:coveragePosition})
console.log(rows)

export default {
  fields: [
    {
      name: 'cell-uid',
      format: '',
      tableFieldIndex: 1,
      type: 'real'
    },
    {name: 'cell_latitude', format: '', tableFieldIndex: 2, type: 'real'},
    {name: 'cell_longitude', format: '', tableFieldIndex: 3, type: 'real'},
    {name: 'cell_power', format: '', tableFieldIndex: 4, type: 'real'},
    {name: 'cell_elevation', format: '', tableFieldIndex: 5, type: 'real'},
    {name: 'coverage_uid', format: '', tableFieldIndex: 6, type: 'real'},
    {name: 'coverage_latitude', format: '', tableFieldIndex: 7, type: 'real'},
    {name: 'coverage_longitude', format: '', tableFieldIndex: 8, type: 'real'},
    {name: 'coverage_radius', format: '', tableFieldIndex: 9, type: 'real'},
    {name: 'coverage_color', format: '', tableFieldIndex: 9, type: 'real'},
    {name: 'coverage_type', format: '', tableFieldIndex: 10, type: 'real'},
    {name: 'coverage_elevation', format: '', tableFieldIndex: 11, type: 'real'}
  ],
  rows: rows
  // rows: [
  //   [
  //     'f81d4fae-7dec-11d0-a765-00a0c91e6bf7',
  //     32.780839,
  //     -96.800806,
  //     100,
  //     100,
  //     'f81d4fae-7dec-11d0-a765-00a0c91e6bf7.01',
  //     32.781506,-96.798878,
  //     70,
  //     [130,245,113],
  //     'coverage_beam',
  //     1
  //   ],
  //     [
  //       'f81d4fae-7dec-11d0-a765-00a0c91e6bf7',
  //       32.780839,
  //       -96.800806,
  //       100,
  //       100,
  //       'f81d4fae-7dec-11d0-a765-00a0c91e6bf7.01',
  //       32.781506,-96.798878,
  //       50,
  //       [130,245,113],
  //       'coverage_beam',
  //       2
  //     ],
  //       [
  //         'f81d4fae-7dec-11d0-a765-00a0c91e6bf7',
  //         32.780839,
  //         -96.800806,
  //         100,
  //         100,
  //         'f81d4fae-7dec-11d0-a765-00a0c91e6bf7.01',
  //         32.781506,-96.798878,
  //         50,
  //         [130,245,113],
  //         'coverage_beam',
  //         3
  //       ],
  //         [
  //           'f81d4fae-7dec-11d0-a765-00a0c91e6bf7',
  //           32.780839,
  //           -96.800806,
  //           100,
  //           100,
  //           'f81d4fae-7dec-11d0-a765-00a0c91e6bf7.01',
  //           32.781506,-96.798878,
  //           50,
  //           [130,245,113],
  //           'coverage_beam',
  //           4
  //         ],
  //           [
  //             'f81d4fae-7dec-11d0-a765-00a0c91e6bf7',
  //             32.780839,
  //             -96.800806,
  //             100,
  //             100,
  //             'f81d4fae-7dec-11d0-a765-00a0c91e6bf7.01',
  //             32.781506,-96.798878,
  //             50,
  //             [130,245,113],
  //             'coverage_beam',
  //             5
  //           ]
  // ]
};
