
import React from 'react';
import styled from 'styled-components';
import viaviLogo from './viavi-white.png'

console.log(viaviLogo)

const StyleLogo = styled.img`
  height: 100px
  src: viaviLogo
  position: absolute;
`;

const Logo = ({ src = viaviLogo, height = 100, alt = "Viavi Solutions Logo" }) => {
  // Import result is the URL of your image
  // return <img src={viaviLogo} alt="logo" />;
  return <StyleLogo className="logo"
    src={src}
    alt={alt}
    height={height}
  />
}

export default Logo;
