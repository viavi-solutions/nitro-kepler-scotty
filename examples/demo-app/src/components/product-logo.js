
import React from 'react';
import styled from 'styled-components';
import productLogo from './5G-logo-white.png'

console.log(productLogo)

const StyleProductLogo = styled.img`
  margin: 40px 0px;
  position: absolute;
`;

const ProductLogo = ({ src = productLogo, height = 80, alt = "5G" }) => {
  // Import result is the URL of your image
  // return <img src={viaviLogo} alt="logo" />;
  return <StyleProductLogo className="product-logo"
    src={src}
    alt={alt}
    height={(height * 0.4)}
  />
}

export default ProductLogo;
