
import React, {Component} from 'react';
import styled from 'styled-components';
import {StyledBtn} from './load-remote-map';
import PropTypes from 'prop-types';
import {addDataToMap, addNotification} from 'kepler.gl/actions';


import subscriberRouteData from '../../data/sample-subscribers-small-set-2';
// import cellCoverageL1Data from './data/sample-cell-coverage-L1';
import cellCoverageData from '../../data/sample-cell-coverage';
import userbeamCoverageData from '../../data/sample-userbeam-coverage';
import configuration from '../../config/config';

const propTypes = {
  // call backs
  onLoad5GData: PropTypes.func.isRequired
};

console.log(StyledBtn)

const StyledFromGroup = styled.div`
  margin-top: 30px;
  display: flex;
  flex-direction: row;
`;

const StyleExample5GDataTab = styled.div`
`;

class Load5GData extends Component{

  // onLoad5GData() {
  //   console.log('do more work');
  //
  //   addDataToMap({
  //     version: "v1",
  //     datasets: [
  //       {
  //         info: {
  //           label: 'Cell Coverage in Dallas, Tx',
  //           id: 'cell_coverage'
  //         },
  //         data: cellCoverageData
  //       },{
  //       info: {
  //         label: 'Subscriber Routes in Dallas, Tx',
  //         id: 'subscriber_routes'
  //       },
  //       data: subscriberRouteData
  //     },{
  //     info: {
  //       label: 'Userbeam Coverage in Dallas, Tx',
  //       id: 'user_beam_coverage'
  //     },
  //     data: userbeamCoverageData
  //   }],
  //
  //     config: configuration
  //   })
  //   console.log('work done');
  // }

  onLoad5GData  = () => {
    this.props.onLoad5GData();
  }

  constructor(props) {
    super(props);
  }

  render() {

    console.log({Load5GData:this.prop})

    return(
      <div className="example-5g-data-tab">
        <h1>Load 5G Data</h1>
        <StyledFromGroup>
          <StyledBtn type="submit" onClick={this.onLoad5GData} >
            Load
          </StyledBtn>
        </StyledFromGroup>
      </div>
    );
  }
}

Load5GData.propTypes = propTypes;

export default Load5GData;
