// Copyright (c) 2019 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import cellCoverageLayerConfig from './layers/cell-coverage-layer-config';
import userBeamCoverageLayerConfig from './layers/user-beam-coverage-layer-config';
import subscriberRoutesLayerConfig from './layers/subscriber-routes-layer-config';
import layerConfig from './layers/layer';

import cellCoverageL1LayerConfig from './layers/cell-coverage-L1-layer-config';

import subscriberRoutesMeasureDatetimeFilterConfig from './filters/subscriber-routes-measurement-datetime-filter-config';

export default {
  version: "v1",
  config: {
    visState: {
      layers: [
        cellCoverageLayerConfig,
        userBeamCoverageLayerConfig,
        // layerConfig,
        subscriberRoutesLayerConfig,
        cellCoverageL1LayerConfig
      ],
      filters: [
        subscriberRoutesMeasureDatetimeFilterConfig
      ],
     interactionConfig:{
        brush:{
           enabled: false,
           size: 0.5
        },
        tooltip:{
           enabled: false,
           fieldsToShow: {
              datetime:[
                 "subscriber_imsi",
                 "subscriber_measurement_datetime",
                 "coverage_beam_uid",
                 "coverage_beam_quality",
                 "subscriber_beam_uid",
                 "subscriber_beam_quality"
              ]
           }
        }
     },
     layerBlending: "additive"
    },
    mapState: {
      "bearing": -50.21815286624204,
      "dragRotate": true,
      "latitude": 32.78063571412189,
      "longitude": -96.79991160953956,
      "pitch": 43.577746385020326,
      "zoom": 17.054310380185548,
      isSplit: false
    },
    mapStyle: {
      styleType: "dark",
      visibleLayerGroups: {
        border: false,
        building: true,
        label: false,
        land: true,
        road: true,
        water: true,
        "3d building": true
      },
      buildingLayer: {
        color: [18, 25, 38],
        isVisible: false,
        opacity: 0.7
      },
      mapStyles: {}
    }
  }
};
