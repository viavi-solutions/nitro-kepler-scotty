// Copyright (c) 2019 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

export default {
          "id": "o0hy75m",
          "type": "heatmap",
          "config": {
            "dataId": "cell_coverage",
            "label": "Ground Coverage",
            "color": [
              48,
              109,
              30
            ],
            "columns": {
              "lat": "coverage_latitude",
              "lng": "coverage_longitude"
            },
            "isVisible": true,
            "visConfig": {
              "opacity": 0.23,
              "colorRange": {
                "name": "ColorBrewer RdYlGn-10",
                "type": "diverging",
                "category": "ColorBrewer",
                "colors": [
                  "#a50026",
                  "#d73027",
                  "#f46d43",
                  "#fdae61",
                  "#fee08b",
                  "#d9ef8b",
                  "#a6d96a",
                  "#66bd63",
                  "#1a9850",
                  "#006837"
                ],
                "reversed": false
              },
              "radius": 100,
              "fixedRadius": true
            },
            "textLabel": {
              "field": null,
              "color": [
                255,
                255,
                255
              ],
              "size": 50,
              "offset": [
                0,
                0
              ],
              "anchor": "middle"
            }
          },
          "visualChannels": {
            "weightField": null,
            "weightScale": "linear",
            "sizeField": {
              "name": "coverage_radius",
              "type": "real"
            },
            "sizeScale": "sqrt"
          }
        }
