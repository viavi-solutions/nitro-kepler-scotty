// Copyright (c) 2019 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

export default {
          "id": "srlc",
          "type": "point",
          "config": {
            "dataId": "subscriber_routes",
            "label": "Subscriber Routes",
            "color": [48, 109, 30],
            "columns": {
              "lat": "subscriber_latitude",
              "lng": "subscriber_longitude",
              "altitude": "subscriber_elevation"
            },
            "isVisible": true,
            "visConfig": {
              "radius": 15,
              "fixedRadius": true,
              "opacity": 0.8,
              "outline": false,
              "thickness": 2,
              "colorRange": {
                  "name": "ColorBrewer RdYlGn-6",
                  "type": "diverging",
                  "category": "ColorBrewer",
                  "colors": [
                    "#440706",
                    "#fc8d59",
                    "#fee08b",
                    "#d9ef8b",
                    "#91cf60",
                    "#1a9850"
                  ],
                  "reversed": false
              },
              "radiusRange": [
                0,
                50
              ],
              "hi-precision": false
            },
            "textLabel": {
              "field": null,
              "color": [
                255,
                255,
                255
              ],
              "size": 50,
              "offset": [
                0,
                0
              ],
              "anchor": "middle"
            }
          },
          "visualChannels": {
            "colorField": {
              "name": "subscriber_beam_quality_color",
              "type": "real"
            },
            "colorScale": "quantize",
            "sizeField": null,
            "sizeScale": "linear"
          }
        }
