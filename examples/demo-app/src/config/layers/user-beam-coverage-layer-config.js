// Copyright (c) 2019 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

export default {
  "id": "ubclc",
  "type": "point",
  "config": {
    "dataId": "user_beam_coverage",
    "label": "User Beam Coverage",
    "color": [48, 109, 30],
    "columns": {
      "lat": "coverage_latitude",
      "lng": "coverage_longitude",
      "altitude": null
    },
    "isVisible": true,
    "visConfig": {
      "radius": 5,
      "fixedRadius": true,
      "opacity": 0.2,
      "outline": true,
      "thickness": 2,
      "colorRange": {
        "name": "Global Warming",
        "type": "sequential",
        "category": "Uber",
        "colors": [
          "#5A1846",
          "#900C3F",
          "#C70039",
          "#E3611C",
          "#F1920E",
          "#FFC300"
        ]
      },
      "radiusRange": [
        0,
        50
      ],
      "hi-precision": false
    },
    "textLabel": {
      "field": null,
      "color": [
        255,
        255,
        255
      ],
      "size": 50,
      "offset": [
        0,
        0
      ],
      "anchor": "middle"
    }
  },
  "visualChannels": {
    "colorField": null,
    "colorScale": "quantile",
    "sizeField": {
      "name": "coverage_radius",
      "type": "real"
    },
    "sizeScale": "sqrt"
  }
}
